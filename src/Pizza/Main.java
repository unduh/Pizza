package Pizza;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.List;

public class Main {
	
	private static void closeAndExit(BufferedReader br) {
	    try {
			br.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
		System.exit(1);
	}
	
	public static void main(String[] args) {
		
		FileReader fr = null;
		int rows = 0;
		int columns = 0;
		int[][] pizza = null;
		/*
		 * T := 0, M := 1
		 */
		int minIngredients = 0;
		int maxCells = 0;
		
		
		try {
			
			
			/*
			 * 
			 * 
			 * Биг дата
			 * 
			 * 
			 */
			
			fr = new FileReader("resource/medium.in");
			
			
		} catch (FileNotFoundException e) {
			System.out.println("File not found!");
    		System.exit(1);
		}
	    BufferedReader br = new BufferedReader(fr);

	    String line = "";

	    try {
	    	line = br.readLine();
			
		} catch (IOException e) {
			System.out.println("First line could not be readed!");
			closeAndExit(br);
		}
	   
    	
    	String[] splited = line.split(" ");

    	if (splited.length != 4) {
    		System.out.println("First line has not enough arguments!");
    		closeAndExit(br);
    	}
    	
    	try {
        	rows = Integer.parseInt(splited[0]);
        	columns = Integer.parseInt(splited[1]);
        	minIngredients = Integer.parseInt(splited[2]);
        	maxCells = Integer.parseInt(splited[3]);
    	} catch(NumberFormatException e) {
    		e.printStackTrace();
    		closeAndExit(br);
    	}
    	
    	pizza = new int[rows][columns];
	    
	    try {
	    	for (int i = 0; i < rows; i++) {
	    		
	    		line = br.readLine();
				if (line == null) {
					System.out.println("Line was null!");
					closeAndExit(br);
				}
				

				char[] lineArray = line.toCharArray();
				if (lineArray.length != columns) {
					System.out.println("Line array had not the right length!");
					closeAndExit(br);
				}
	    		
				for (int j = 0; j < columns; j++) {
					if (lineArray[j] == 'T') {
						pizza[i][j] = 0;	
					} else if (lineArray[j] == 'M') {
						pizza[i][j] = 1;
					} else {
						System.out.println("The ingredient is not tomato or mushroom!");
						closeAndExit(br);
					}
				}
	    		
	    		
	    	}
		} catch (IOException e) {
			e.printStackTrace();
			closeAndExit(br);
		}
	    
	    //Pizza p = new Pizza(pizza, minIngredients, maxCells);
	    
	    System.out.println("rows: " + rows + ", columns: " + columns);
	    System.out.println("min ingredients: " + minIngredients + ", max cells: " + maxCells);
	    
	    
	    
	    
	    
	    /*
	     * здесь можешь делить пиццу
	     * 
	     */
	    int div = (int) (maxCells * 2);
	    
	    int rowPs = rows / div;
	    if (rows - rowPs * div >= maxCells || rowPs == 0) {
	    	rowPs++;
	    }
	    int colPs = columns / div;
	    if (columns - colPs * div >= maxCells || colPs == 0) {
	    	colPs++;
	    }
	    
	    System.out.println("Общее количество кусочков: " + rowPs * colPs);

	    List<List<int[]>> list = new ArrayList<List<int[]>>();
	    int numOfSlices = 0;
	    
	    
	    int[][] slicedPizza = new int[rows][columns];
	    int pizzaNum = 0;
	    		
	    int sum = 0;
	    boolean toPrint = true;
	    for (int i = 0; i < rowPs; i++) {
	    	for (int j = 0; j < colPs; j++) {
	    		
	    		int top = i * div;
	    		int bottom = top + div - 1;
	    		
	    		int left = j * div;
	    		int right = left + div - 1;
	    		
	    		if (i == rowPs - 1) {
	    			bottom = rows - 1;
	    		} 
    			if (j == colPs - 1) {
    				right = columns - 1;
    			} 
	    		
    			int[][] result = new int[bottom - top + 1][right - left + 1];
    			for (int i2 = 0; i2 < result.length; i2++) {
    				for (int j2 = 0; j2 < result[0].length; j2++) {
    					result[i2][j2] = pizza[top + i2][left + j2];
    					slicedPizza[top + i2][left + j2] = pizzaNum;
    				}
    			}
    			
    			
    			
    			/*
    			 * 
    			 * МОЖНО ОГРАНИЧИТЬ КАКОЙ КУСОЧЕК РЕЗАТЬ, true - ОЗНАЧАЕТ ВСЕ КУСОЧКИ
    			 * pizzaNum - номер кусочка, который можно увидеть в output/numberedPizza.txt
    			 * 
    			 */
    			
    			if (true) {
        			//printMatrix(result);
        			toPrint = false;
        			
        			int[][] matrix = new int[bottom - top + 1][right - left + 1];
        			System.out.print("Cells: " + matrix.length * matrix[0].length + ", ");
        			Pizza p = new Pizza(result, minIngredients, maxCells);
        			//System.out.println(p.toString());
        			try {
        				p.setMustReturn(false);
        				
        				
        				/*
        				 * 
        				 * 3 аргумент означает сколько ячеек можно не учитывать (наподобе эпсилон)
        				 * 
        				 */
        				
        				int epsilon = 0;
        				
        				p.generateSlices4(epsilon);
        				//p.generateSlices3(matrix, null, epsilon, System.currentTimeMillis());
        				
        				int value = p.getValueOfList(p.getListWithMaxValue(p.getListOfMaxLists())); 
        				System.out.println("value: " + value + ", check: " + p.check());
        				//int value = p.getValueOfList(p.getMaxList());
        				//p.printMaxList();
        				//p.printCountAndLvl();
        				//p.printSliceList(2);
        				sum += value;
        	    	} catch (StackOverflowError e) {
        	    		System.out.println("OHJHHAASDFHAHSDF: STACKOVERFLOW!!!");
        	    		System.exit(1);
        	    	} 
        			//list.add(p.getMaxList());
        			//list.add(p.getListWithMaxValue(p.getListOfMaxLists()));
        			//numOfSlices += p.getMaxList().size();
        			//numOfSlices += p.getListWithMaxValue(p.getListOfMaxLists()).size();
    			}
    	    	

	    		pizzaNum++;
	    	}
	    }
	    
	    System.out.println("УПУЩЕННЫЕ ЯЧЕЙКИ: " + (rows * columns - sum));
	    
	    writeToFileMatrix(slicedPizza, pizza);
	    
	    writeToFileSlices(list, numOfSlices);
	   
	    
	    
	    //System.out.println(p.toString());
	    //System.out.println(((int) Math.pow((int) Math.sqrt(9), 2)) / 2);
	    //p.printPart();
	    //p.changeToValid();
	    //System.out.println(p.getValueOfCells(0, 0, 2, 1));
    	//int[][] matrix = new int[rows][columns];
	    /*try {
		    p.printListWithMaxValue(p.generateSlices2(matrix, null, 1, minIngredients));
	    } catch (StackOverflowError e) {
	    	System.out.println(e.getMessage());
	    } catch (InterruptedException e) {
	    	System.out.println(e.getMessage());
		}*/

	}
	
	private static void writeToFileSlices(List<List<int[]>> list, int size) {
		if (list == null) {
			return;
		}
		try{
		    PrintWriter writer = new PrintWriter("output/slicedPizza.txt", "UTF-8");
		    
		    writer.println(size);

			for (List<int[]> smallList : list) {
				for (int[] slice : smallList) {
					String s = "";
					for (int i = 0; i < slice.length; i++) {
						s += slice[i];
						if (i < slice.length - 1) {
							s += " ";
						}
					}
				    writer.println(s);
				}
			}
			
		    
		    writer.close();
		} catch (IOException e) {
		   // do something
			System.out.println("Error while writing to file!");
		}
	}

	public static void printMatrix(int[][] matrix) {
		if (matrix == null) {
			return;
		}
		String s = "";
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[0].length; j++) {
				if (matrix[i][j] < 10) {
					s += " ";
				}
				if (matrix[i][j] < 100) {
					s += " ";
				}
				s += matrix[i][j] + " ";
			}
			s += "\n";
		}
		System.out.println(s);
	}
	
	public static void writeToFileMatrix(int[][] matrix, int[][] pizza) {
		if (matrix == null) {
			return;
		}
		try{
		    PrintWriter writer = new PrintWriter("output/numberedPizza.txt", "UTF-8");

			for (int i = 0; i < matrix.length; i++) {
				String s = "";
				for (int j = 0; j < matrix[0].length; j++) {
					if (matrix[i][j] < 10) {
						s += " ";
					}
					if (matrix[i][j] < 100) {
						s += " ";
					}
					//s += matrix[i][j] + "|" + pizza[i][j] + " ";
					s += matrix[i][j] + " ";
				}
			    writer.println(s);
			}
			
		    
		    writer.close();
		} catch (IOException e) {
		   // do something
			System.out.println("Error while writing to file!");
		}
	}
}
