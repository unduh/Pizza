package Pizza;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

public class Pizza {
	
	private int rows;
	private int columns;
	private int[][] pizza;
	private int minIngredients;
	private int maxCells;
	private int[][] id;
	private List<int[]> positions;
	private boolean  mustReturn = false;
	private boolean[] mustReturn4thread;
	private int iterations;
	private List<int[]> maxList;
	private ArrayList<int[]>[] listOfMaxLists;
	private Thread[] threads;
	private List<Integer> levels;
	private int count = 0;
	private int lvl = 0;
	Lock lock = new ReentrantLock(false);
	
	/*
	 * 
	 * 
	 * ЗДЕСЬ МОЖНО ОГРАНИЧИТЬ ВРЕМЯ ВЫСЧИТЫВАНИЯ
	 * 
	 */
	private final long TIMEOUT = 25000;
	
	
	public Pizza(int[][] pizza, int minIngredients, int maxCells) {
		this.pizza = pizza;
		this.minIngredients = minIngredients;
		this.maxCells = maxCells;
		rows = pizza.length;
		columns = pizza[0].length;
		initId();
		initPositions();
		iterations = 0;
		maxList = new ArrayList<int[]>();
		levels = new ArrayList<Integer>();
	}

	private void initPositions() {
		positions = new ArrayList<int[]>();
		for (int i = 1; i <= maxCells; i++) {
			for (int j = 1; j <= maxCells; j++) {
				if (i * j == maxCells) {
					positions.add(new int[] {i - 1, j - 1});
				}
			}
		}
	}
	
	public List<int[]> getMaxList() {
		return maxList;
	}

	private void initId() {
		id = new int[rows][pizza[0].length];
		int inc = 0;
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				id[i][j] = inc;
				inc++;
			}
		}
	}
	
	public ArrayList<int[]> getListWithMaxValue(ArrayList<int[]>[] arrayLists) {
		ArrayList<int[]> list = null;
		if (arrayLists == null) {
			return list;
		}
		int max = 0;
		for (int i = 0; i < arrayLists.length; i++) {
			int listMax = 0;
			if (arrayLists[i] == null) {
				return null;
			}
			for (int j = 0; j < arrayLists[i].size(); j++) {
				if (arrayLists[i].get(j).length == 4) {
					listMax += getValueOfCells(arrayLists[i].get(j)[0], arrayLists[i].get(j)[1], 
							arrayLists[i].get(j)[2], arrayLists[i].get(j)[3]);
				}
			}
			if (listMax > max) {
				max = listMax;
				list = arrayLists[i];
			}
		}
		return list;
	}
	
	public void printListWithMaxValue(ArrayList<int[]>[] slices) {
		//System.out.println("Size: " + slices.size());
		ArrayList<int[]> list = getListWithMaxValue(slices);
		String s = "";
		if (list == null) {
			System.out.println("There was a null-list!");
			return;
		}
		for (int i = 0; i < list.size(); i++) {
			if (list.get(i).length != 4) {
				continue;
			}
			s += "( ";
			for (int j = 0; j < 4; j++) {
				s += list.get(i)[j] + " ";
			}
			s += ")";
		}
		//System.out.println(s);
	}
	
	public void setMustReturn(boolean to) {
		mustReturn = to;
	}
	
	
	public List<List<int[]>> generateSlices2(int[][] toSliceIn, List<List<int[]>> list, int recursionLvl, int numOfLists, int epsilon) throws InterruptedException {
		//System.out.println("Anfang:");
		//printMatrix(toSliceIn);
		//System.out.println("numOfLists: " + numOfLists);
		//System.out.println("epsilon: " + epsilon);
		for (int level : levels) {
			if (recursionLvl == level) {
				return list;
			}
		}
		if (mustReturn) {
			return list;
		}
		if (list != null) {
			// we have found enough lists
			List<int[]> slices = list.get(list.size() - 1); 
			if (numOfLists == list.size() && list.size() > 0) {
				if (slices != null) {
					if (slices.size() > 0 && getValueOfList(slices) >= rows * columns - epsilon) {
						//printListWithMaxValue(list);
						slices.add(new int[] {0});
						//return list;
						mustReturn = true;
						return list;
					}
				}
			// last list has enough
			} else if (numOfLists < list.size() && list.size() > 0) {
				if (slices != null) {
					if (getValueOfList(slices) >= rows * columns - epsilon) {
						// we have found one solution
						slices.add(new int[] {0});
					}
				}
			}
		}
		
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				if (toSliceIn[i][j] == 0) {
					//not part of slice

					int[][] sortedPositions = sortPositions(toSliceIn, i, j);
					
					int lastI2 = -1;
					int lastJ2 = -1;
					for (int p = 0; p < sortedPositions.length; p++) {

						int i2 = sortedPositions[p][0];
						int j2 = sortedPositions[p][1];
						if (lastI2 == i2 && lastJ2 == j2) {
							continue;
						}
						lastI2 = i2;
						lastJ2 = j2;
						int[] slice = new int[] {i, j, i2, j2};
						int value = getValueOfCells(i, j, i2, j2);
						
						/*
						if (value == 0 && ((i2 - i + 1) * (j2 - j + 1) <= 2 * minIngredients)) {
							return list;
						}
						*/
						
						
						//System.out.println("Slice: (" + i + ", " + j + ", " + i2 + ", " + j2 + ")");
						
						
						// change slice cells from 0 to 1, i2 and j2 inclusive
						//int counter = 
						System.out.println("Wende slice an: ");
						printMatrix(toSliceIn);
						changeCellsTo(toSliceIn, i, j, i2, j2, 1);
						printMatrix(toSliceIn);

						/*if (value == 0 && counter > 0 && rows - i > maxCells && columns - j > maxCells) {
							changeCellsTo(toSliceIn, i, j, i2, j2, 0);
							continue;
						}*/
						
						
						List<int[]> sliceList = new ArrayList<int[]>();
						if (list == null) {
							list = new ArrayList<List<int[]>>();
						}  
						
						// add slice to last list
						if (list.size() == 0) {
							sliceList.add(slice);
							list.add(sliceList);
						} else {
							List<int[]> lastList = list.get(list.size() - 1);
							
							// we do not change the previous solution
							if (lastList.size() == 0) {
								lastList.add(slice);
							} else if (lastList.size() > 0 && lastList.get(lastList.size() - 1).length == 1) {
								sliceList.add(slice);
								list.add(sliceList);
							} else {
								lastList.add(slice);
							}
							/*String s = "temp: \n";
							for (int k = 0; k < temp.size(); k++) {
								for (int k2 = 0; k2 < temp.get(k).length; k2++) {
									s += temp.get(k)[k2] + " ";
								}
								s += "\n";
							}
							System.out.println(s);
							TimeUnit.MILLISECONDS.sleep(3000);*/
						}
						

						/*iterations++;
						if (iterations % 100 == 0) {
							System.out.println(iterations);
						}*/
						
						
						
						//depth first search
						/*if (i == 0 && j == 3 && i2 ==  2 && j2 == 4) {
							System.out.println("lsg");
							TimeUnit.MILLISECONDS.sleep(3000);
						}
						TimeUnit.MILLISECONDS.sleep(100);*/
						
						list = generateSlices2(toSliceIn, list, recursionLvl + 1, numOfLists, epsilon);
						if (mustReturn) {
							return list;
						}
						
						
						//undo position and list changes
						//System.out.println("Undo slice: ");
						//printMatrix(toSliceIn);
						changeCellsTo(toSliceIn, i, j, i2, j2, 0);
						//printMatrix(toSliceIn);
						if (list.size() > 0) {
							List<int[]> lastList = list.get(list.size() - 1); 
							// we do not change the previous solution
							if (lastList.size() > 0 && lastList.get(lastList.size() - 1).length != 1) {
								lastList.remove(slice);
							} 
						}
						
						
						
						/*List<List<int[]>> resultAtPos = generateSlices(toSlice);

						
						// result += positionSlice + another list from resultAtPos
						result.addAll(resultAtPos);*/
					}
				}
			}
		}
		
		
		
		if (recursionLvl == 0) {
			List<List<int[]>> toReturn = new ArrayList<List<int[]>>();
			toReturn.add(maxList);
			return toReturn;
		} else {
			levels.add(recursionLvl);
		}
		
		if (list.size() > 0) {

			List<int[]> lastList = list.get(list.size() - 1);
			if (lastList.size() > 0) {
				if (maxList.size() == 0) {
					maxList = new ArrayList<int[]>();
					maxList.addAll(lastList);
				} else {
					if (getValueOfList(maxList) < getValueOfList(lastList)) {
						maxList = new ArrayList<int[]>();
						maxList.addAll(lastList);
					}
				}
			} 
		}	
		return list;
	}
	
	
	public void printCountAndLvl() {
		System.out.println("count: " + count + ", lvl: " + lvl);
	}
	
	
	public void generateSlices3(int[][] toSliceIn, List<int[]> list, int epsilon, long start) {
		
		lvl++;
		
		if (mustReturn) {
			return;
		}
		
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				if (toSliceIn[i][j] == 0) {
					//not part of slice

					int[][] sortedPositions = sortPositions(toSliceIn, i, j);
					
					for (int p = 0; p < sortedPositions.length; p++) {
						
						count++;
						
						
						if (System.currentTimeMillis() - start > TIMEOUT) {
							return;
						}

						int i2 = sortedPositions[p][0];
						int j2 = sortedPositions[p][1];
						int[] slice = new int[] {i, j, i2, j2};
						int value = getValueOfCells(i, j, i2, j2);

						if (value == 0 && ((i2 - i + 1) * (j2 - j + 1) <= minIngredients)) {
							return;
						}
						
						if (list == null) {
							list = new ArrayList<int[]>();
						}

						//System.out.println("Wende slice an: ");
						//printMatrix(toSliceIn);
						changeCellsTo(toSliceIn, i, j, i2, j2, 1);
						/*try {
							TimeUnit.MILLISECONDS.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println("Nach dem slice: ");
						printMatrix(toSliceIn);*/
						list.add(slice);
						

						if (getValueOfList(list) >= rows * columns - epsilon) {
							mustReturn = true;
							maxList = new ArrayList<int[]>();
							maxList.addAll(list);
							return;
						}
						
						if (maxList == null) {
							maxList = new ArrayList<int[]>();
							maxList.addAll(list);
						} else {
							if (getValueOfList(list) > getValueOfList(maxList)) {
								maxList = new ArrayList<int[]>();
								maxList.addAll(list);
							}
						}
						
						
						// into depth
						generateSlices3(toSliceIn, list, epsilon, start);
						
						changeCellsTo(toSliceIn, i, j, i2, j2, 0);
						list.remove(slice);
					}
				}
			}
		}
		return;
	}
	

	@SuppressWarnings("unchecked")
	public void generateSlices4(int epsilon) {
		int[][] sortedPositions = sortPositions(new int[rows][columns], 0, 0);
		mustReturn4thread = new boolean[sortedPositions.length];
		threads = new Thread[sortedPositions.length];
		listOfMaxLists = (ArrayList<int[]>[]) new ArrayList[sortedPositions.length];
		for (int i = 0; i < sortedPositions.length; i++) {
			ArrayList<int[]> list = new ArrayList<int[]>();
			listOfMaxLists[i] = list;
			int[][] toSlice = new int[rows][columns];
			int i2 = sortedPositions[i][0];
			int j2 = sortedPositions[i][1];
			int[] slice = new int[] {0, 0, i2, j2};
			list.add(slice);
			changeCellsTo(toSlice, 0, 0, i2, j2, 1);
			threads[i] = new Thread(new GenPos(toSlice, epsilon, i));

			threads[i].start();
		}
		for (int i = 0; i < threads.length; i++) {
			try {
				threads[i].join();
			} catch (InterruptedException e) {
				e.printStackTrace();
			}
		}
	}

	public void generateSlices4thread(int[][] toSliceIn, List<int[]> list, int epsilon, int index, long start) {


		boolean toReturn = false;
		lock.lock();
		try {
			toReturn = mustReturn;
		} finally {
			lock.unlock();
		}
		
		if (mustReturn4thread[index] || toReturn) {
			return;
		}
		
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				if (toSliceIn[i][j] == 0) {
					//not part of slice

					int[][] sortedPositions = sortPositions(toSliceIn, i, j);

					for (int p = 0; p < sortedPositions.length; p++) {
						
						
						if (System.currentTimeMillis() - start > TIMEOUT) {
							return;
						}

						int i2 = sortedPositions[p][0];
						int j2 = sortedPositions[p][1];
						
						
						int[] slice = new int[] {i, j, i2, j2};
						int value = getValueOfCells(i, j, i2, j2);
						
						if ((i2 < i || j2 < j) && p < sortedPositions.length - 1) {
							continue;
						}
						
						if (((i2 - i + 1) * (j2 - j + 1) < 2 * minIngredients) && p < sortedPositions.length - 1) {
							continue;
						}

						/*if (value == 0 && ((i2 - i + 1) * (j2 - j + 1) <= maxCells)) {
							return;
						}*/
						/*if (value == 0) {
							//i = Math.max(0, i - maxCells);
							//j = Math.max(0, j - maxCells);
							if (p == sortedPositions.length - 1 && getValueOfList(listOfMaxLists[index]) > rows * columns - 3 * maxCells) {
								return;
							}
							continue;
						}*/
						
						if (list == null) {
							list = new ArrayList<int[]>();
							list.add(listOfMaxLists[index].get(0));
						}

						//System.out.println("Index: " + index + ", Wende slice an: ");
						//printMatrix(toSliceIn);
						changeCellsTo(toSliceIn, i, j, i2, j2, 1);
						/*try {
							TimeUnit.MILLISECONDS.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						System.out.println("Index: " + index + ", Nach dem slice: ");
						printMatrix(toSliceIn);*/
						if (!list.contains(slice)) {
							list.add(slice);
						}

						if (getValueOfList(list) >= rows * columns - epsilon) {
							mustReturn4thread[index] = true;
							listOfMaxLists[index] = new ArrayList<int[]>();
							for (int[] l : list) {
								if (!listOfMaxLists[index].contains(l)) {
									listOfMaxLists[index].add(l);
								}
							}
							//listOfMaxLists[index].addAll(list);
							/*try {
								TimeUnit.MILLISECONDS.sleep(2000);
							} catch (InterruptedException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}*/
							lock.lock();
							try {
								mustReturn = true;
							} finally {
								lock.unlock();
							}
							return;
						} else if (getValueOfList(list) > getValueOfList(listOfMaxLists[index])) {
							listOfMaxLists[index] = new ArrayList<int[]>();
							listOfMaxLists[index].addAll(list);
						}
						
						/*if (getValueOfList(list) >= rows * columns - minIngredients) {
							return;
						}*/
						/*try {
							TimeUnit.MILLISECONDS.sleep(2000);
						} catch (InterruptedException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						printSliceList(index);*/
						// into depth
						generateSlices4thread(toSliceIn, list, epsilon, index, start);
						
						toReturn = false;
						lock.lock();
						try {
							toReturn = mustReturn;
						} finally {
							lock.unlock();
						}
						
						if (mustReturn4thread[index] || toReturn) {
							return;
						}
						
						changeCellsTo(toSliceIn, i, j, i2, j2, 0);
						list.remove(slice);
					}
				}
			}
		}
		return;
	}
	
	
	public void printSliceList(int index) {
		String s = "Index: " + index + ", List: ";
		if (listOfMaxLists[index] == null) {
			s += "null";
			System.out.println(s);
			return;
		}
		for (int[] i : listOfMaxLists[index]) {
			if (i == null) {
				s += "null";
				System.out.println(s);
				return;
			}
			for (int j = 0; j < i.length; j++) {
				if (j == 0) {
					s += "( ";
				}
				s += i[j] + " ";
				if (j == i.length - 1) {
					s += ")";
				}
			}
		}
		s += ", Value: " + getValueOfList(listOfMaxLists[index]);
		System.out.println(s);
	}


	private int[][] sortPositions(int[][] toSliceIn, int i, int j) {
		int[][] result = new int[positions.size()][positions.get(0).length];
		int[] posValue = new int[positions.size()];
		for (int p = 0; p < positions.size(); p++) {
			
			int i2 = Math.min(positions.get(p)[0] + i, rows - 1);
			for (int k = i; k <= i2; k++) {				
				if (toSliceIn[k][j] == 1) {
					i2 = k - 1;
				}
			}
			int j2 = Math.min(positions.get(p)[1] + j, columns - 1);
			for (int k = j; k <= j2; k++) {
				if (toSliceIn[i][k] == 1) {
					j2 = k - 1;
				}
			}
			
			posValue[p] = getValueOfCells(i, j, i2, j2);
			result[p] = new int[]{i2, j2};
		}

		// sort
		for (int k1 = 0; k1 < result.length; k1++) {
			int max = posValue[k1];
			int[] maxPos = result[k1];
			int index = k1;
			for (int k2 = k1 + 1; k2 < result.length; k2++) {
				if (posValue[k2] > max) {
					max = posValue[k2];
					maxPos = result[k2];
					index = k2;
				}
			}
			posValue[index] = posValue[k1];
			result[index] = result[k1];
			posValue[k1] = max;
			result[k1] = maxPos;
		}
		return result;
	}
	
	
	

	private int changeCellsTo(int[][] toSliceIn, int i, int j, int i2, int j2,	int changeTo) {
		int counter = 0;
		for (int k1 = i; k1 <= i2; k1++) {
			for (int k2 = j; k2 <= j2; k2++) {
				toSliceIn[k1][k2] = changeTo;
				counter++;
			}
		}
		return counter;
	}

	public List<List<int[]>> generateSlices(int[][] toSliceIn) {
		

		List<List<int[]>> result = new ArrayList<List<int[]>>();
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				if (toSliceIn[i][j] == 0) {
					//not part of slice

					for (int p = 0; p < positions.size(); p++) {
						int[][] toSlice = new int[rows][columns];
						for (int k1 = 0; k1 < rows; k1++) {
							for (int k2 = 0; k2 < columns; k2++) {
								toSlice[k1][k2] = toSliceIn[k1][k2];
							}
						}
						
						int i2 = Math.min(positions.get(p)[0] + i, rows - 1);
						for (int k = i; k <= i2; k++) {
							if (toSlice[k][j] == 1) {
								i2 = k - 1;
							}
						}
						int j2 = Math.min(positions.get(p)[1] + j, columns - 1);
						for (int k = j; k <= j2; k++) {
							if (toSlice[i][k] == 1) {
								j2 = k - 1;
							}
						}
						
						int counter = 0;
						// change slice cells from 0 to 1, i2 and j2 inclusive
						for (int k1 = i; k1 <= i2; k1++) {
							for (int k2 = j; k2 <= j2; k2++) {
								toSlice[k1][k2] = 1;
								counter++;
							}
						}
						
						int[] slice = new int[] {i, j, i2, j2};
						int value = getValueOfCells(i, j, i2, j2);
						if (value == 0 && counter > 0 && rows - i > maxCells && columns - j > maxCells) {
							iterations--;
							continue;
						}
						iterations++;
						
						if (iterations % 100 == 0) {
							System.out.println(iterations);
						}
						
						//recursive: find all lists
						List<List<int[]>> resultAtPos = generateSlices(toSlice);

						if (resultAtPos == null || resultAtPos.size() == 0) {
							resultAtPos = new ArrayList<List<int[]>>();
							List<int[]> sliceList = new ArrayList<int[]>();
							sliceList.add(slice);
							resultAtPos.add(sliceList);
						}
						else {
							for (int k = 0; k < resultAtPos.size(); k++) {
								resultAtPos.get(k).add(0, slice);
								
							}
						}
						
						// result += positionSlice + another list from resultAtPos
						result.addAll(resultAtPos);
					}
					return result;
				}
			}
		}
		return result;
	}
	
	public void printMatrix(int[][] matrix) {
		String s = "";
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < columns; j++) {
				s += matrix[i][j] + " ";
			}
			s += "\n";
		}
		System.out.println(s);
	}
	
	
	public void printGeneratedSlices() {
		String s = "";
		List<List<int[]>> list = generateSlices(new int[rows][columns]);
		for (int i = 0; i < list.size(); i++) {
			s += i + ": ";
			for (int j = 0; j < list.get(i).size(); j++) {
				s += "(";
				for (int k = 0; k < list.get(i).get(j).length; k++) {
					if (k == list.get(i).get(j).length - 1) {
						s += list.get(i).get(j)[k]; 
					} else {
						s += list.get(i).get(j)[k] + ", "; 
					}
				}
				s += ") ";
			}
			s += "\n";
		}
		System.out.println(s);
	}
	
	
	public int getValueOfList(List<int[]> input) {
		int result = 0;
		if (input == null) {
			System.out.println("getValueOfList: NullPointerException!");
			return result;
		}
		for (int i = 0; i < input.size(); i++) {
			result += getValueOfCells(input.get(i)[0], input.get(i)[1], input.get(i)[2], input.get(i)[3]);
		}
		return result;
	}
	
	
	public int getValueOfCells(int r1, int c1, int r2, int c2) {
		int tomatos = 0;
		int mushrooms = 0;
		int cells = 0;
		for (int i = r1; i <= r2; i++) {
			for (int j = c1; j <= c2; j++) {
				if (pizza[i][j] == 0) {
					tomatos++;
				} else {
					mushrooms++;
				}
				cells++;
			}
		}
		if (tomatos >= minIngredients && mushrooms >= minIngredients && cells <= maxCells) {
			return cells;
		}
		return 0;
	}
	
	public String slicePizza() {
		String s = "";
		return s;
	}

	
	/*
	 * T := 0, M := 1
	 */
	public String toString() {
		String s = pizza.length + " " + pizza[0].length 
				+ " " + minIngredients + " " + maxCells + "\n";
		for (int i = 0; i < pizza.length; i++) {
			for (int j = 0; j < pizza[0].length; j++) {
				if (pizza[i][j] == 0) {
					s += "T";
				} else {
					s += "M";
				}
			}
			s += "\n";
		}
		return s;
	}
	
	public void printPositions() {
		String s = "";
		for (int i = 0; i < positions.size(); i++) {
			if (i == positions.size() - 1) {
				s += "(" + positions.get(i)[0] + ", " + positions.get(i)[1] + ")";
			} else {
				s += "(" + positions.get(i)[0] + ", " + positions.get(i)[1] + "), ";
			}
		}
		System.out.println(s);
	}
	
	private class GenPos implements Runnable {
		
		int posIndex;
		int epsilon;
		int[][] matrix;
		
		GenPos(int[][] matrix, int epsilon, int posIndex) {
			this.posIndex = posIndex;
			this.epsilon = epsilon;
			this.matrix = matrix;
		}
		
		public void run() {
			generateSlices4thread(matrix, null, epsilon, posIndex, System.currentTimeMillis());
		}
	}

	public ArrayList<int[]>[] getListOfMaxLists() {
		return listOfMaxLists;
	}

	public void printMaxList() {
		String s = "length: " + positions.size() + "\n";
		for (int[] i : maxList) {
			if (i == null) {
				s += "null";
				System.out.println(s);
				return;
			}
			for (int j = 0; j < i.length; j++) {
				if (j == 0) {
					s += "( ";
				}
				s += i[j] + " ";
				if (j == i.length - 1) {
					s += ")";
				}
			}
		}
		System.out.println(s);
	}
	
	
	public boolean check() {
		int[][] ref = new int[rows][columns];
		List<int[]> list = getListWithMaxValue(listOfMaxLists);
		for (int[] slice : list) {
			if (slice.length != 4) {
				return false;
			}
			for (int i = slice[0]; i < slice[2]; i++) {
				for (int j = slice[1]; j < slice[3]; j++) {
					if (i == 30 && j == 7 && ref[i][j] == 0) {
						System.out.println("first: i: " + slice[0] + ", j: " + slice[1] + ", i2: " + slice[2] + ", j2: " + slice[3]);
					}
					if (ref[i][j] == -1) {
						System.out.println("actI: " + i + ", actJ: " + j);
						System.out.println("second: i: " + slice[0] + ", j: " + slice[1] + ", i2: " + slice[2] + ", j2: " + slice[3]);
						return false;
					}
					ref[i][j] = -1;
				}
			}
		}
		return true;
	}
}


